<!DOCTYPE html>
<html data-wf-site="5327d91251afcf551e0002d8">
<head>
<meta charset="utf-8">
<title>Valothaki-Ambulance</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="assets/css/valothaki.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<script>
	WebFont.load({
		google : {
			families : [ "Montserrat:400,700", "Bitter:400,700" ]
		}
	});
</script>
<link rel="shortcut icon" type="x-icon" href="assets/images/favicon.ico">
<link rel="apple-touch-icon"
	href="https://y7v4p6k4.ssl.hwcdn.net/51d1bb05fc804b2621000001/51e06f0756878bb26a000008_webclip-slate.png">
</head>
<body>
	<div class="header-section" style="height: 400px;">

		<div class="w-container container">
			<h1 class="otherh1">
				<span id="cf3300ea-24f5-42d1-a357-49d86bcba2f7"
					class="GINGER_SOFTWARE_mark">Valo</span> <span
					id="44ec2f17-eb64-4060-ad94-ab96a3586a4f"
					class="GINGER_SOFTWARE_mark">Thaki</span>
			</h1>
			<div style="float: right;">
				<a class="button buttonround redbutton"
					href="http://localhost:8080/ValothakiWebsite/hospital"></a> <a
					class="button buttonround tealbutton"
					href="http://localhost:8080/ValothakiWebsite/doctor"></a> <a
					class="button buttonround pureredbutton"
					href="http://localhost:8080/ValothakiWebsite/bloodbank"></a> <a
					class="button buttonround violetbutton"
					href="http://localhost:8080/ValothakiWebsite/doctorassistant"></a>
				<a class="button buttonround logoutbutton"
					href="http://localhost:8080/ValothakiWebsite/sign_in"
					style="background-color: #0277bd;"></a>
			</div>
		</div>

		<div class="w-form sign-up-form"
			style="width: 50%; margin-left: auto; margin-right: auto;">
			<form class="w-clearfix" name="wf-form-signup-form"
				data-name="Signup Form" method="post">
				<input class="w-input field" type="text" id="Name"
					placeholder="Ambulance Name" name="name" data-name="Name"
					required="required" autofocus="autofocus"> <input
					class="w-input field" id="Phone" type="tel"
					placeholder="Contact Number" name="phone" data-name="Phone"
					required="required"> <input class="w-input field" id="Rate"
					type="number" placeholder="Rate" name="rate" data-name="Rate">
				<input class="w-input field" id="District" type="text"
					placeholder="District" name="district" data-name="district"
					required="required"> <input class="w-button button"
					type="submit" value="Done" data-wait="Please wait..."
					style="width: 30%; height: 60px; margin-top: 10px; margin-left: 1px; background-color: #0277bd;">
				<a class="w-button button"
					href="http://localhost:8080/ValothakiWebsite/ambulance"
					style="background-color: #2F292B; padding-top: 20px; width: 20%; height: 38px; margin-top: 10px; margin-left: 10px; font-size: 16px; font-weight: 300; text-align: center; text-decoration: none; text-shadow: rgba(0, 0, 0, 0.18) 0px 1px 0px;">Cancel</a>
			</form>

		</div>
	</div>

	<div class="footer-section">
		<div class="w-container">
			<div class="w-row">
				<div class="w-col w-col-6 w-col-small-6">
					<div class="copyright">� 2014 Cyan Studio. All right
						reserved.&nbsp;</div>
				</div>

			</div>
		</div>
	</div>
</body>
</html>