
<!DOCTYPE html>
<html data-wf-site="5327d91251afcf551e0002d8">
<head>
<meta charset="utf-8">
<title>Valo Thaki</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css"
	href="assets/css/valothaki_home.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<script>
	WebFont.load({
		google : {
			families : [ "Montserrat:400,700", "Bitter:400,700" ]
		}
	});
</script>
<link rel="shortcut icon" type="x-icon" href="assets/images/favicon.ico">
<link rel="apple-touch-icon"
	href="https://y7v4p6k4.ssl.hwcdn.net/51d1bb05fc804b2621000001/51e06f0756878bb26a000008_webclip-slate.png">
</head>
<body>
	<div class="header-section">
		<div class="w-container container">
			<h1>
				<span id="cf3300ea-24f5-42d1-a357-49d86bcba2f7"
					class="GINGER_SOFTWARE_mark">Valo</span> <span
					id="44ec2f17-eb64-4060-ad94-ab96a3586a4f"
					class="GINGER_SOFTWARE_mark">Thaki</span>
			</h1>
			<p class="subtitle">
				<span id="27a8acd1-5d92-4f59-b07b-01d7ccb94fbc"
					class="GINGER_SOFTWARE_mark">live</span> your life
			</p>
			<div class="w-form sign-up-form">
				<form class="w-clearfix" name="wf-form-signup-form"
					data-name="Signup Form" data-redirect="/menu" action=""
					method="post" >
					<input class="w-input field" type="email"
						placeholder="Enter your email address" name="email"
						data-name="Email" required="required" autofocus="autofocus">
					<input class="w-input field" id="Password" type="password"
						placeholder="Enter password" name="password" data-name="Password"
						required="required"> <input class="w-button button"
						type="submit" value="Log In" name="login_submit"
						data-wait="Please wait...">
				</form>

			</div>
		</div>
	</div>
	<div class="footer-section">
		<div class="w-container">
			<div class="w-row">
				<div class="w-col w-col-6 w-col-small-6">
					<div class="copyright">� 2014 Cyan Studio. All right
						reserved.&nbsp;</div>
				</div>

			</div>
		</div>
	</div>
</body>
</html>