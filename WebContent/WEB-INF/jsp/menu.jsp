<!DOCTYPE html>
<html data-wf-site="5327d91251afcf551e0002d8">
<head>
<meta charset="utf-8">
<title>Valothaki-Ambulance</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="assets/css/valothaki.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
<script>
	WebFont.load({
		google : {
			families : [ "Montserrat:400,700", "Bitter:400,700" ]
		}
	});
</script>
<link rel="shortcut icon" type="x-icon" href="assets/images/favicon.ico">
<link rel="apple-touch-icon"
	href="https://y7v4p6k4.ssl.hwcdn.net/51d1bb05fc804b2621000001/51e06f0756878bb26a000008_webclip-slate.png">
</head>
<body>
	<div class="header-section" style="height: 350px;">

		<div class="w-container container">
			<h1 class="otherh1">
				<span id="cf3300ea-24f5-42d1-a357-49d86bcba2f7"
					class="GINGER_SOFTWARE_mark">Valo</span> <span
					id="44ec2f17-eb64-4060-ad94-ab96a3586a4f"
					class="GINGER_SOFTWARE_mark">Thaki</span>
			</h1>
			<div style="float: right;">

				<a class="button buttonround logoutbutton"
					href="http://localhost:8080/ValothakiWebsite/sign_in"
					style="background-color: #0277bd;"></a>
			</div>
		</div>

		<div class="w-form sign-up-form"
			style="width: 100%; margin-left: auto; margin-right: auto; margin-top: 240px;">
			<div style="margin-left: auto; margin-right: auto; width: 75%;">
				<a class="button buttonround redbutton"
					href="http://localhost:8080/ValothakiWebsite/hospital"
					style="width: 100px; height: 150px; border-radius: 150px; background-size: 80px; "></a>
				<a class="button buttonround tealbutton"
					href="http://localhost:8080/ValothakiWebsite/doctor"
					style="width: 100px; height: 150px; border-radius: 150px; background-size: 80px; "></a>
				<a class="button buttonround bluebutton"
					href="http://localhost:8080/ValothakiWebsite/ambulance"
					style="width: 100px; height: 150px; border-radius: 150px; background-size: 80px; "></a>
				<a class="button buttonround pureredbutton"
					href="http://localhost:8080/ValothakiWebsite/bloodbank"
					style="width: 100px; height: 150px; border-radius: 150px; background-size: 80px;"></a>
				<a class="button buttonround violetbutton"
					href="http://localhost:8080/ValothakiWebsite/doctorassistant"
					style="width: 100px; height: 150px; border-radius: 150px; background-size: 80px; "></a>
			</div>


		</div>
	</div>

	<div class="footer-section">
		<div class="w-container">
			<div class="w-row">
				<div class="w-col w-col-6 w-col-small-6">
					<div class="copyright">� 2014 Cyan Studio. All right
						reserved.&nbsp;</div>
				</div>

			</div>
		</div>
	</div>
</body>
</html>