package com.cyan.valothaki.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cyan.valothaki.database.connection.DatabaseConnection;

@WebServlet("/hospital_add")
@SuppressWarnings("serial")
public class HospitalAddController extends HttpServlet {

	Connection con = null;
	Statement stmt = null;
	ResultSet rs1 = null;
	ResultSet rs2 = null;
	ResultSet rs3 = null;
	ResultSet rs4 = null;
	PreparedStatement pst = null;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/jsp/hospital_add.jsp");

		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String name, phone, house_road, area, city, district;
		int areaId = 1;
		int districtId = 1;

		name = (req.getParameter("name"));
		phone = (req.getParameter("phone"));
		house_road = (req.getParameter("house_road"));
		area = (req.getParameter("area"));
		city = (req.getParameter("city"));
		district = (req.getParameter("district"));

		try {
			DatabaseConnection dbconnect = new DatabaseConnection();
			con = dbconnect.getConnection();
			try {
				String sql1 = "select DistrictId from District where Name="
						+"'"+ district+"'";
				stmt = con.createStatement();
				rs1 = stmt.executeQuery(sql1);

				if (rs1.next()) {
					districtId = rs1.getInt(1);
				} else {
					String sql2 = "INSERT INTO District(Name) VALUES ('"
							+ district + "')";
					pst = con.prepareStatement(sql2);
					pst.execute();

					stmt = con.createStatement();
					rs2 = stmt.executeQuery(sql1);

					if (rs2.next()) {
						districtId = rs2.getInt(1);
					}
				}

				String sql3 = "select AreaId from Area where Name='"+ area
						+ "' and DistrictId='" + districtId+"'";
				stmt = con.createStatement();
				rs3 = stmt.executeQuery(sql3);

				if (rs3.next()) {
					areaId = rs3.getInt(1);
				} else {
					String sql4 = "INSERT INTO Area(Name,DistrictId) VALUES ('" + area
							+ "','"+districtId+"')";
					pst = con.prepareStatement(sql4);
					pst.execute();

					stmt = con.createStatement();
					rs4 = stmt.executeQuery(sql3);
					if (rs4.next()) {
						areaId = rs4.getInt(1);
					}
				}

				String address = house_road + "," + area + "," + city + ","
						+ district;
				String sql5 = "INSERT INTO hospital(Name, Phone, Address, AreaId, DistrictId) VALUES ('"
						+ name
						+ "','"
						+ phone
						+ "','"
						+ address
						+ "','"
						+ areaId + "','" + districtId + "')";
				pst = con.prepareStatement(sql5);
				pst.execute();

				resp.sendRedirect("http://localhost:8080/ValothakiWebsite/hospital");
			} catch (Exception e) {
				System.out.println("sql connection error: " + e);
			}
		} catch (Exception e) {
			System.out.println("database connection error: " + e);

		}
	}

}
