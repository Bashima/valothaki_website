package com.cyan.valothaki.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.sql.Result;
import javax.servlet.jsp.jstl.sql.ResultSupport;

import com.cyan.valothaki.database.connection.DatabaseConnection;

@SuppressWarnings("serial")
@WebServlet("/doctorassistant")
public class DoctorAssistantController extends HttpServlet {

	private Connection connection;
	private Statement statement;
	private ResultSet resultSet;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Result result = null;
		try {
			DatabaseConnection dbconnect = new DatabaseConnection();
			connection = dbconnect.getConnection();
			try {
				String query = "select distinct(u.firstName), u.email, u.phone, u.road, u1.firstName, c.Name from doctor d, user u, user u1, chamber c, DoctorAssistant da where u.userId=da.AssistantUserId and da.DoctorUserId=u1.userId and da.ChamberId=c.ChamberId and u.type=2 and u1.type=3";
				statement = connection.createStatement();
				resultSet = statement.executeQuery(query);
				
			} catch (Exception e) {
				System.out.println("statement error:  " + e);
			}
		} catch (Exception e) {
			System.out.println("database connection error: " + e);
		}
		result = ResultSupport.toResult(resultSet);
		req.setAttribute("result", result);
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/jsp/doctorassistant.jsp");

		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	}

}
