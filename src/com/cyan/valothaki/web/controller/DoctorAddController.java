package com.cyan.valothaki.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cyan.valothaki.database.connection.DatabaseConnection;

@WebServlet("/doctor_add")
@SuppressWarnings("serial")
public class DoctorAddController extends HttpServlet {

	Connection con = null;
	Statement stmt = null;
	ResultSet rs1 = null;
	ResultSet rs2 = null;
	ResultSet rs3 = null;
	ResultSet rs4 = null;
	ResultSet rs5 = null;
	ResultSet rs6 = null;
	ResultSet rs7 = null;
	ResultSet rs8 = null;
	PreparedStatement pst = null;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/jsp/doctor_add.jsp");

		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String name, phone, speciality, degree, hospital, district;
		int specialityId=1;
		int hospitalId=1;
		int districtId=1;
		int userId=1;

		name = (req.getParameter("name"));
		phone = (req.getParameter("phone"));
		speciality = (req.getParameter("speciality"));
		degree = (req.getParameter("degree"));
		hospital = (req.getParameter("hospital"));
		district = (req.getParameter("district"));

		try {
			DatabaseConnection dbconnect = new DatabaseConnection();
			con = dbconnect.getConnection();
			try {
				String sql1 = "select DistrictId from District where Name='"
						+ district+"'";
				stmt = con.createStatement();
				rs1 = stmt.executeQuery(sql1);

				if(rs1.next())
				{
					districtId = rs1.getInt(1);
				}
				else {
					String sql2 = "INSERT INTO District(Name) VALUES ('"
							+ district + "')";
					pst = con.prepareStatement(sql2);
					pst.execute();

					stmt = con.createStatement();
					rs2 = stmt.executeQuery(sql1);
					if(rs2.next())
					{
						districtId = rs2.getInt(1);
					}
				}

				

				String sql3 = "select HospitalId from hospital where Name='"
						+ hospital + "' and DistrictId='" + districtId+"'";
				stmt = con.createStatement();
				rs3 = stmt.executeQuery(sql3);

				if (rs3.next()) {
					hospitalId = rs3.getInt(1);
					
					String sql4 = "select SpecializationId from Specialization where Name='"
							+ speciality+"'";
					stmt = con.createStatement();
					rs4 = stmt.executeQuery(sql4);

					if(rs4.next())
					{
						specialityId = rs4.getInt(1);
					}
					else{
						String sql5 = "INSERT INTO Specialization(Name) VALUES ('"
								+ speciality + "')";
						pst = con.prepareStatement(sql5);
						pst.execute();

						stmt = con.createStatement();
						rs5 = stmt.executeQuery(sql4);

						if(rs5.next())
						{
							specialityId = rs5.getInt(1);
						}
					}

					
					String sql6= "INSERT INTO user(firstName, Phone, email, password, status, type) VALUES ('"
							+ name  + "','"+phone+"','"+name+"@gmail.com','"+"1234"+ "','"+"active"+ "','"+"3"+"')";
					pst = con.prepareStatement(sql6);
					pst.execute();
					String sql7 = "select UserId from user where firstName='"
							+ name+"' and Phone='"+phone+"'";
					stmt = con.createStatement();
					rs6 = stmt.executeQuery(sql7);

					if (rs6.next()) {
						userId = rs6.getInt(1);
						String sql8= "INSERT INTO doctor(DoctorUserId, Qualification, SpecializationId, HospitalId) VALUES ('"
								+ userId + "','"+degree+"','"+specialityId+"','"+hospitalId+"')";
						pst = con.prepareStatement(sql8);
						pst.execute();
						
						resp.sendRedirect("http://localhost:8080/ValothakiWebsite/doctor");
					}
					else
					{
						resp.sendRedirect("http://localhost:8080/ValothakiWebsite/error_doc_user");
					}
					
					

				} else {
					resp.sendRedirect("http://localhost:8080/ValothakiWebsite/error_doc_add");
				}

			} catch (Exception e) {
				System.out.println("sql connection error: " + e);
			}
		} catch (Exception e) {
			System.out.println("database connection error: " + e);

		}
	}

}
