package com.cyan.valothaki.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.sql.Result;
import javax.servlet.jsp.jstl.sql.ResultSupport;

import com.cyan.valothaki.database.connection.DatabaseConnection;

@SuppressWarnings("serial")
@WebServlet("/hospital")
public class HospitalController extends HttpServlet {

	Statement statement = null;

	ResultSet resultSet = null;

	Connection connection = null;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)

	throws ServletException, IOException {
		Result result = null;
		try {
			DatabaseConnection dbconnect = new DatabaseConnection();
			connection = dbconnect.getConnection();
			try {
				String query = "select h.Name, Address, d.Name, Phone from District d, hospital h where h.DistrictId=d.DistrictId";
				statement = connection.createStatement();
				resultSet = statement.executeQuery(query);
			} catch (Exception e) {
				System.out.println("statement error:  " + e);
			}
		} catch (Exception e) {
			System.out.println("database connection error: " + e);
		}
		result = ResultSupport.toResult(resultSet);
		req.setAttribute("result", result);
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/jsp/hospital.jsp");

		dispatcher.forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)

	throws ServletException, IOException {

	}

}
