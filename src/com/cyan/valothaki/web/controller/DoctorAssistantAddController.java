package com.cyan.valothaki.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cyan.valothaki.database.connection.DatabaseConnection;

@WebServlet("/doctorassistant_add")
@SuppressWarnings("serial")
public class DoctorAssistantAddController extends HttpServlet {

	Connection con = null;
	Statement stmt = null;
	ResultSet rs1 = null;
	ResultSet rs2 = null;
	ResultSet rs3 = null;
	ResultSet rs4 = null;
	PreparedStatement pst = null;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/jsp/doctorassistant_add.jsp");

		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String name, phone, email, address, doctor, chamber, password;
		int doctorId = 1;
		int chamberId = 1;
		int doctorUserId = 1;
		int assistantUserId = 1;

		name = (req.getParameter("name"));
		phone = (req.getParameter("phone"));
		email = (req.getParameter("email"));
		address = (req.getParameter("address"));
		doctor = (req.getParameter("doctor"));
		chamber = (req.getParameter("chamber"));

		try {
			DatabaseConnection dbconnect = new DatabaseConnection();
			con = dbconnect.getConnection();
			try {
				String sql1 = "select DoctorId, DoctorUserId from doctor,user  where firstName='"
						+ doctor + "'";
				stmt = con.createStatement();
				rs1 = stmt.executeQuery(sql1);

				if (rs1.next()) {

					doctorId = rs1.getInt(1);
					doctorUserId = rs1.getInt(2);

					String sql2 = "select ChamberId from chamber where Name='"
							+ chamber +"'";
					stmt = con.createStatement();
					rs2 = stmt.executeQuery(sql2);

					if (rs2.next()) {
						chamberId = rs2.getInt(1);
					} else {
						String sql3 = "INSERT INTO chamber(Name, DoctorUserId) VALUES ('"
								+ chamber + "','" + doctorUserId + "')";
						pst = con.prepareStatement(sql3);
						pst.execute();

						stmt = con.createStatement();
						rs3 = stmt.executeQuery(sql2);

						if (rs3.next()) {
							chamberId = rs3.getInt(1);
						}

					}

					password = phone + email;

					String sql4 = "INSERT INTO user(firstName, Phone, email, password, status, type, road) VALUES ('"
							+ name  + "','"+phone+"','"+email+"','"+password+ "','"+"active"+ "','"+"2"+ "','"+address+"')";
					pst = con.prepareStatement(sql4);
					pst.execute();
					
					String sql5 = "select userId from user where firstName='"
							+ name + "' and Phone='" + phone+"'";
					stmt = con.createStatement();
					rs4 = stmt.executeQuery(sql5);

					
					if (rs4.next()) {
					
						assistantUserId = rs4.getInt(1);
					String sql6= "insert into DoctorAssistant(AssistantUserId, DoctorUserId, ChamberId) values ('"
							+assistantUserId+"','"+doctorUserId+"','"+chamberId+"')";
						pst = con.prepareStatement(sql6);
						pst.execute();

						resp.sendRedirect("http://localhost:8080/ValothakiWebsite/doctorassistant");
				}

				} else {
					resp.sendRedirect("http://localhost:8080/ValothakiWebsite/error_doc_add");
				}

			} catch (Exception e) {
				System.out.println("sql connection error: " + e);
			}
		} catch (Exception e) {
			System.out.println("database connection error: " + e);

		}
	}
}
