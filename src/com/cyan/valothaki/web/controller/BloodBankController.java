package com.cyan.valothaki.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.sql.Result;
import javax.servlet.jsp.jstl.sql.ResultSupport;

import com.cyan.valothaki.database.connection.DatabaseConnection;

@WebServlet("/bloodbank")
@SuppressWarnings("serial")
public class BloodBankController extends HttpServlet{

	private Connection connection;
	private Statement statement;
	private ResultSet resultSet;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		 Result result = null;
		 try {
		 DatabaseConnection dbconnect = new DatabaseConnection();
		 connection = dbconnect.getConnection();
		 try {
		 String query =
		 "select b.Name, b.Address, b.Phone, d.Name from District d, bloodbank b where b.DistrictId=d.DistrictId";
		 statement = connection.createStatement();
		 resultSet = statement.executeQuery(query);
		 } catch (Exception e) {
		 System.out.println("statement error:  " + e);
		 }
		 } catch (Exception e) {
		 System.out.println("database connection error: " + e);
		
		 }
		 result = ResultSupport.toResult(resultSet);
		 req.setAttribute("result", result);
		 RequestDispatcher dispatcher =
		 req.getRequestDispatcher("/WEB-INF/jsp/bloodbank.jsp");

		 dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	}

	
}
