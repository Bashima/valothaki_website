package com.cyan.valothaki.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cyan.valothaki.database.connection.DatabaseConnection;

@WebServlet("/ambulance_edit")
@SuppressWarnings("serial")
public class AmbulanceEditController extends HttpServlet {

	Connection con = null;
	Statement stmt = null;
	ResultSet rs = null;
	PreparedStatement pst = null;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/jsp/ambulance_edit.jsp");

		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String name, phone, rate, district;
		int districtId;

		name = (req.getParameter("name"));
		phone = (req.getParameter("phone"));
		rate = (req.getParameter("rate"));
		district = (req.getParameter("district"));

		try {
			if (!name.equals(null) || !phone.equals(null) || !rate.equals(null)
					|| !district.equals(null)) {
				DatabaseConnection dbconnect = new DatabaseConnection();
				con = dbconnect.getConnection();
				try {
					if (!district.equals(null)) {
						String sql1 = "select DistrictId from District where Name="
								+ district;
						stmt = con.createStatement();
						rs = stmt.executeQuery(sql1);

						if (rs.equals(null)) {
							String sql2 = "INSERT INTO District(Name) VALUES ('"
									+ district + "')";
							pst = con.prepareStatement(sql2);
							pst.execute();

							stmt = con.createStatement();
							rs = stmt.executeQuery(sql1);

						}

						districtId = rs.getInt(1);
						 String sql = "UPDATE ambulance " +
				                   "SET DistrictId ="+districtId+" WHERE id in (100, 101)";
				      stmt.executeUpdate(sql);
					}
//					String sql3 = "INSERT INTO ambulance(Name,Phone,Fee,DistrictId) VALUES ('"
//							+ name
//							+ "','"
//							+ phone
//							+ "','"
//							+ rate
//							+ "','"
//							+ districtId + "')";
//					pst = con.prepareStatement(sql3);
//					pst.execute();

					resp.sendRedirect("http://localhost:8080/ValothakiWebsite/ambulance");
				} catch (Exception e) {

				}

			} else {
				resp.sendRedirect("http://localhost:8080/ValothakiWebsite/ambulance");
			}

		} catch (Exception e) {
			System.out.println("database connection error: " + e);

		}
	}
}
