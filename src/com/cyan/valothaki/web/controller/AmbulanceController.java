package com.cyan.valothaki.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.sql.Result;
import javax.servlet.jsp.jstl.sql.ResultSupport;

import com.cyan.valothaki.database.connection.DatabaseConnection;

@WebServlet("/ambulance")
@SuppressWarnings("serial")
public class AmbulanceController extends HttpServlet {

	Connection connection;
	Statement statement;
	ResultSet resultSet;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		Result result = null;

		try {
			DatabaseConnection dbconnect = new DatabaseConnection();
			connection = dbconnect.getConnection();
			try {
				String query = "select a.Name, Fee, Phone, d.Name, ar.Name  from District d, Area ar,ambulance a  where a.DistrictId=d.DistrictId and a.AreaId=ar.AreaId";
				statement = connection.createStatement();
				resultSet = statement.executeQuery(query);

			} catch (Exception e) {
				System.out.println("statement error:  " + e);
			}
		} catch (Exception e) {
			System.out.println("database connection error: " + e);
		}

		result = ResultSupport.toResult(resultSet);

		req.setAttribute("result", result);

		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/jsp/ambulance.jsp");

		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	}

}
