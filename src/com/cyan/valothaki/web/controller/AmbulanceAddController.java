package com.cyan.valothaki.web.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cyan.valothaki.database.connection.DatabaseConnection;

@WebServlet("/ambulance_add")
@SuppressWarnings("serial")
public class AmbulanceAddController extends HttpServlet {

	Connection con = null;
	Statement stmt = null;
	ResultSet rs1 = null;
	ResultSet rs2 = null;
	ResultSet rs3 = null;
	PreparedStatement pst = null;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = req
				.getRequestDispatcher("/WEB-INF/jsp/ambulance_add.jsp");

		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String name, phone, rate, district;
		int districtId=0;

		name = (req.getParameter("name"));
		phone = (req.getParameter("phone"));
		rate = (req.getParameter("rate"));
		district = (req.getParameter("district"));

		try {
			DatabaseConnection dbconnect = new DatabaseConnection();
			con = dbconnect.getConnection();
			try {
				String sql1 = "select DistrictId from District where Name='"
						+ district+"'";
				
				stmt = con.createStatement();
				rs1 = stmt.executeQuery(sql1);

				if (rs1.next()) {
					districtId= rs1.getInt(1);
					}
				else
				{
					String sql2 = "INSERT INTO District(Name) VALUES ('"
							+ district + "')";
					pst = con.prepareStatement(sql2);
					pst.execute();
					
					stmt = con.createStatement();
					rs2 = stmt.executeQuery(sql1);
					if(rs2.next())
					districtId= rs2.getInt(1);
				}
				
				String sql3= "INSERT INTO ambulance(Name,Phone,Fee,DistrictId,AreaId,HospitalId) VALUES ('"
						+ name + "','"+phone+"','"+rate+"','"+1+"','"+1+"','"+districtId+"')";
				pst = con.prepareStatement(sql3);
				pst.execute();
				
				resp.sendRedirect("http://localhost:8080/ValothakiWebsite/ambulance");
			} catch (Exception e) {
				System.out.println("sql error: " + e);
			}
		} catch (Exception e) {
			System.out.println("database connection error: " + e);

		}
	}

}
