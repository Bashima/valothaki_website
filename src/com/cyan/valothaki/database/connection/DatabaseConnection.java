package com.cyan.valothaki.database.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

	Connection con = null;
	public Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager
					.getConnection("jdbc:mysql://localhost:3306/Android","root", "");
		} catch (ClassNotFoundException e) {
			System.out.println("Error 1: "+e);
		} catch (SQLException e) {
			System.out.println("Error 2: "+e);
		} 
		return con;
	}
}
